
from pathlib import Path
from collections.abc import Iterable
import sys

import numpy as np

from matplotlib.font_manager import FontManager, FontProperties
import matplotlib.pyplot as plt

from PIL import Image, ImageFont, ImageDraw

from skimage import feature

import torch
from torch.utils.data._utils.collate import default_collate

from .datasets.clusters import scale_to_square
from .datasets.masks import bg_img_mask
from .datasets.v2 import poly_to_crop, adjust_cluster_to_crop, adjust_crop_to_image, combine_edges_and_bg

from .utils import np_to_shapely, shapely_to_np
from .text import project_text


if '../pytorch-CycleGAN-and-pix2pix' not in sys.path:
    sys.path.insert(0, '../pytorch-CycleGAN-and-pix2pix')

if 'pytorch-CycleGAN-and-pix2pix' not in sys.path:
    sys.path.insert(0, 'pytorch-CycleGAN-and-pix2pix')

from util.util import tensor2im
from data.tx_dataset import preprocess, merge_patches, TxDataset

from boltons.iterutils import chunked
from tqdm.autonotebook import tqdm
from tqdm import tqdm as std_tqdm


def _normalize_fonts(num, fonts):

    def _normalize_font(font):
        if font == None:
            font = FontProperties()
        if isinstance(font, FontProperties):            
            font = FontManager().findfont(font)
        if isinstance(font, (str, Path)):
            font = ImageFont.truetype(font, size=40)
        return font

    if not isinstance(fonts, Iterable):
        fonts = [fonts]*num

    fonts = [_normalize_font(font) for font in fonts]
    return fonts

def _normalize_texts(num, texts):
    if isinstance(texts, str):
        texts = [texts]*num
    return texts

def _normalize_opt(opt):
    if opt is None:
        opt = build_opt()
    return opt

def _normalize_args(polys_np, texts, fonts):
    num = len(polys_np)
    fonts = _normalize_fonts(num, fonts)
    texts = _normalize_texts(num, texts)

    return polys_np, texts, fonts

def _normalize_verbose(verbose):

    if isinstance(verbose, std_tqdm):
        say = verbose.set_description
        verbose = True
    elif verbose:
        say = print
    else:
        say = lambda x: x

    return say, verbose

def text_content_image(img, polys_np, texts, fonts=None):

    polys_np, texts, fonts = _normalize_args(polys_np, texts, fonts)

    bg_mask, masks = bg_img_mask(img.size, polys_np, rectify_poly=False)
    
    imgs = np.stack([ 
        project_text(img.size, text, poly_np, font=font) 
            for poly_np, text, font in zip(polys_np, texts, fonts)
    ])
    text_img = 255 - ((255 - imgs).sum(0) > 0).astype('uint8')*255

    text_edges = feature.canny(text_img[..., 0]/255, sigma=.0) 

    img_with_edges = combine_edges_and_bg(text_edges, np.array(img), bg_mask)


    img_with_edges = Image.fromarray((img_with_edges*255).astype('uint8'))
    bg_mask = Image.fromarray(bg_mask.astype('uint8')*255)
    masks = [Image.fromarray(m.astype('uint8')*255) for m in masks]
    
    return img_with_edges, bg_mask, masks


def assamble_cluster(crop, crop_poly_id, polys_sh):

    mask = np.zeros(len(polys_sh), dtype=bool)
    
    for i, p in enumerate(polys_sh):
        if crop_poly_id == i:
            mask[i] = True
        # p = p.minimum_rotated_rectangle
        elif p.is_valid and p.intersection(crop).area  > p.area * 0.8:            
            mask[i] = True

    return mask


def data_for_inpaint(img, polys_np, texts, fonts=None, main_poly_id=0, border_size=80):

    polys_np, texts, fonts = _normalize_args(polys_np, texts, fonts)

    image_w, image_h = img.size
    
    polys_sh = [
        np_to_shapely(poly_np).minimum_rotated_rectangle 
            for poly_np in polys_np
    ]

    crop = poly_to_crop(polys_sh[main_poly_id], image_w, image_h, border_size=border_size)
                
    mask = assamble_cluster(crop, main_poly_id, polys_sh)


    cluster  = [poly_sh for poly_sh, take in zip(polys_sh, mask) if take]
    cl_texts = [text for text, take in zip(texts, mask) if take]
    cl_fonts = [font for font, take in zip(fonts, mask) if take]


    cluster = adjust_cluster_to_crop(crop, cluster)
    cluster = [shapely_to_np(poly_sh) for poly_sh in cluster]
    

    style_img = img.copy().convert('RGB').crop(box=crop.bounds)
    cont_img  = img.copy().convert('RGB').crop(box=crop.bounds)

    cont_img, bg_mask, patches = text_content_image(cont_img, cluster, cl_texts, fonts=cl_fonts)


    return style_img, cont_img, bg_mask, patches, mask, crop


def build_batch_seq(img, polys_np, texts, fonts=None, 

                        border_size=80, opt=None, verbose=True):

    polys_np, texts, fonts = _normalize_args(polys_np, texts, fonts)
    opt = _normalize_opt(opt)
    # say, verbose = _normalize_verbose(verbose)

    batch_seq = []
    paste_data = []

    if verbose:
        bar = tqdm(total=len(texts))
        bar.update(0)
    while len(texts) > 0:
        style_img, cont_img, bg_mask, patches, mask, crop = data_for_inpaint(img, polys_np, texts, fonts=fonts, border_size=border_size)

        paste_data.append({
            'crop' : crop, 
            'size' : cont_img.size, 
            'polys_np' : 
                [poly_np for poly_np, taken in zip(polys_np, mask) if taken]            
        })

        polys_np = [poly_np for poly_np, taken in zip(polys_np, mask) if not taken]
        texts = [text for text, taken in zip(texts, mask) if not taken]
        fonts = [font for font, taken in zip(fonts, mask) if not taken]

        cont_img, style_img, bg_mask, patches, *_ = preprocess(cont_img, style_img, bg_mask, patches, None, opt)

        patches = merge_patches(patches)

        batch_seq.append({
            'A': cont_img, 
            'B': style_img, 
            'A_paths': [], 
            'mask' : bg_mask, 
            'patches' : patches
        })

        if verbose:
            bar.update(n=mask.sum())

    return batch_seq, paste_data


def paste_batch_seq(img, net_preds, batch_seq, paste_data, crop_poly=True, paste_order=None):

    paste_imgs = []
    for i, (batch, pd) in enumerate(zip(batch_seq, paste_data)):

        net_pred = tensor2im(net_preds[i].unsqueeze(0))
        crop = pd['crop']
        minx, miny, maxx, maxy = map(int, crop.bounds)

        if crop_poly:
            bg_mask = batch['mask'].permute(1,2,0).numpy()            
            style_img = np.array(img.crop(box=crop.bounds).resize(net_pred.shape[:2]))
            net_pred = (net_pred * (1 - bg_mask)  + style_img * bg_mask).astype('uint8')

        net_pred = Image.fromarray(net_pred.astype('uint8'))
        net_pred = net_pred.convert('RGB').resize(pd['size'])

        img.paste(net_pred, box=(minx, miny))



def forward_model(batch_seq,

                    batch_size=10, opt=None, model=None, 
                    verbose=True):

    say, verbose = _normalize_verbose(verbose)    

    if model is None:
        say('Building model')
        model = build_model(opt=opt)

    net_pred = []
    for i, batch in enumerate(chunked(batch_seq, batch_size)):
        say(f'Precessing batch {i}')
        batch = TxDataset.collate(batch)
        with torch.no_grad():
            model.set_input(batch)
            model.forward()
        net_pred.append(model.fake_B.clone())

    return torch.cat(net_pred, dim=0)


def inpaint_texts(img, polys_np, texts, fonts=None, 

                    opt=None, border_size=80, batch_size=10, 
                    crop_poly=True, model=None, verbose=True,
                    paste_order = None, memory=None                    
                    ):
    
    
    say, _verbose = _normalize_verbose(verbose)
    polys_np, texts, fonts = _normalize_args(polys_np, texts, fonts)
    opt = _normalize_opt(opt)

    say('Building batch_seq')

    _build_batch_seq  = build_batch_seq
    if memory is not None:
        _build_batch_seq  = memory.cache(build_batch_seq)    

    
    batch_seq, paste_data = _build_batch_seq(img, polys_np, texts, fonts, 
            border_size=border_size, opt=opt, verbose=_verbose)
    say(f'batch_seq_len = {len(batch_seq)}')
    say('Forwarding model')
    net_pred = forward_model(batch_seq, batch_size=batch_size, opt=opt, model=model, verbose=verbose)

    inp_img = img.copy()
    say(f'Pasting results')
    paste_batch_seq(inp_img, net_pred, batch_seq, paste_data, 
                    crop_poly=crop_poly, paste_order=paste_order)

    return inp_img, net_pred, batch_seq, paste_data

from tqdm.autonotebook import tqdm



def _inpaint_cache_dir(net_cfg=None):
    base_dir = Path('~/data/tx/inpaint-cache').expanduser()
    
    if net_cfg is None:
        return base_dir

    dir_name = net_cfg['name']
    if 'netG' in net_cfg:
        dir_name += '-netG-' + net_cfg['netG']
        
    if 'epoch' in net_cfg:
        dir_name += '-epoch-' + str(net_cfg['epoch'])
            
    dir_name = base_dir / dir_name 
    
    return dir_name


from joblib import dump, load
from .nbs_helpers import build_opt, build_model

def inpaint_net_cfg(exist_ok=False, skip_existing=False,  memory=None, data=[], net_cfg=None):
    
    print(net_cfg)

    dir_name = _inpaint_cache_dir(net_cfg)
        

    dir_name.mkdir(exist_ok=exist_ok)
    
    dump(net_cfg, dir_name/f"net_cfg.pkl")
    
    opt = build_opt(**net_cfg)    
    model = build_model(opt=opt)
    
    bar = tqdm(data)
    for ix, img, polys_np in bar:
        if (dir_name/f"inp_img-{ix}.jpg").exists() and skip_existing:
            continue
            
        inp_img, net_pred, batch_seq, paste_data = inpaint_texts(img, polys_np, 'hello', model=model, 
                                                                 memory=memory, verbose=bar)        
        
        inp_img.save(dir_name/f"inp_img-{ix}.jpg")
    
        dump((net_pred, batch_seq, paste_data), dir_name/f"inp_param-{ix}.pkl")




    return dir_name
