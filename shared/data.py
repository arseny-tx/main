from PIL import Image, ImageFont, ImageDraw

import os
import numpy as np

import cv2

from shapely.geometry import box
# from data.base_dataset import BaseDataset, get_params, get_transform
# import util.util as util





from skimage.transform import resize
from scipy.misc import imresize
import string



class DefCharMap:
    VOID_CHAR = '❌'    
    CHARS = string.printable
    SUB_MAP = {}
    DROP_MAP = {}

    @classmethod
    def normalize_char(cls, char):
        if char in cls.DROP_MAP:
            return ' '
        if char in cls.SUB_MAP:
            return cls.SUB_MAP[char]

        return char

    @classmethod
    def normalize_str(cls, str):
        return ''.join(cls.normalize_char(c) for c in str)

    @classmethod
    def char_to_label(cls, char):

        if char in cls.DROP_MAP:
            return 0
        if char in cls.SUB_MAP:
            return cls.char_to_label(cls.SUB_MAP[char])

        assert char in set(cls.CHARS) | {cls.VOID_CHAR}, f"Char {char}, not found"

        if char == cls.VOID_CHAR:
            return 0
        else:
            return 1 + cls.CHARS.index(char)

    @classmethod
    def label_to_char(cls, label):
        pass


class LatinMltCharMap(DefCharMap):
    CHARS = '!"#$%&\'()*+,-./0123456789:;?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]_abcdefghijklmnopqrstuvwxyz+'

    SUB_MAP = {
        'é' : 'e',
        'à' : 'a', 'á' : 'a',
        'Ê' : 'E', 'É' : 'E',

        '·' : '.',
    }

    DROP_MAP = 'حال°£€€إ'



def project_seg_map(img_size, text, poly_np, char_map = DefCharMap):
    
    bg_color = char_map.char_to_label(char_map.VOID_CHAR)
    src_img = np.array([
        [ char_map.char_to_label(t) for t in text ]
    ])
    
    # src_img = resize(src_img, (10, 10), anti_aliasing=False, preserve_range=True)
    src_img = imresize(src_img, (100, 100), interp='nearest')
    w, h = src_img.shape
    src_poly_np = np.array([
        [0, 0], [h, 0], 
        [h, w],  
        [0, w]
    ])

    h, status = cv2.findHomography(src_poly_np, poly_np)
    
    return cv2.warpPerspective(src_img, h, img_size, borderValue=bg_color)


def mk_seg_map(img, polys_np, labels, langs, char_map = LatinMltCharMap):
    
    img_w, img_h = img.size

    bg_masks = [
        mask_from_poly(img.size, poly_np) 
            for poly_np in polys_np
    ]

    for i, bg_mask in enumerate(bg_masks):
        for j in range(i+1, len(bg_masks)):
            intersec = bg_masks[i] & bg_masks[j]
            if intersec.sum() > 0:
                bg_masks[i] &= (~intersec) 


    seg_map = np.zeros((img_h, img_w))
    res_bg_mask = np.zeros((img_h, img_w), np.bool)
    inp_map = np.zeros((3, img_h, img_w))

    for bg_mask, poly_np, label, lang in zip(bg_masks, polys_np, labels, langs):
        # remove possible intersections
        if label == '' or label =='###':
            continue
        if lang != 'Latin':
            continue

        seg_map += bg_mask * project_seg_map(img.size, label, poly_np, char_map = char_map)# .transpose(1, 2, 0)


        inp_map += bg_mask * project_text(img.size, char_map.normalize_str(label), poly_np).transpose(2, 0, 1)
        res_bg_mask |= bg_mask

    seg_map = Image.fromarray(seg_map)

    

    img = np.array(img).transpose(2, 0, 1)

    bg_map = ((1-res_bg_mask) * img) # 

    inp_map += bg_map

    bg_map = Image.fromarray(bg_map.transpose(1, 2, 0).astype(np.uint8))
    inp_map = Image.fromarray(inp_map.transpose(1, 2, 0).astype(np.uint8))
    res_bg_mask = Image.fromarray(res_bg_mask)

    return seg_map, bg_map, res_bg_mask, inp_map
