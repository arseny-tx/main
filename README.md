# Realistic Text Inpainting 




<table>
    <tr>
        <th> <centre> Generator architecture </centre></th>
        <th> <centre> Training data  </centre></th>
    </tr>
    <tr>
        <td>
            <img src="https://docs.google.com/drawings/d/e/2PACX-1vTJ8xV2mijUP65x7pIpzSqEA1vmxl3eHBSZ4xY61xkjn74-7FaRTNHIaWX7GJTQmn-rUV_vwBdl91so/pub?w=952&amp;h=362">
        </td>
        <td>
            <img src="https://docs.google.com/drawings/d/e/2PACX-1vSmpu6OHlDfhLGUyAOmKTtBuDuWtOPdzyewk_AJRE10xtkg3uDHQodZe3b6XF15idi9DSuFRk3OyIo_/pub?w=952&amp;h=481">
        </td>
    </tr>
</table>


The goal of this project is to allow to realistically replace text in arbitrary images. This has a varied number of applications including AR and data augmentation.

In order to train the network data form text detection datasets is used. The areas delimited by the text bounding boxes are substituted with an edge maps. Such images serve as content images specifying the required text inpainting content. In order to supply style information we use a modification of AdaIn adapted to handle the situation where more then one style is present in the image.
As style images the original images are used. 



**Code status:** Work in progress

**Paper:** Work in progress

# Example Results
<table>
    <tr>
        <td><img src="imgs/img1.png"></td>
        <td><img src="imgs/img2.png"></td>
    </tr>
    <tr>
        <td><img src="imgs/img3.png"></td>
        <td><img src="imgs/img4.png"></td>
    </tr>
</table>


# PatchedAdaIn

Our modification of AdaIn could be implemented effectively with broadcasting.
The AdaIn layer consists of two steps, first the features are normalized by substracting the mean and variance then the features are
modulated by adding the reqired mean and variance. We extend the layer to allow modulation with different styles.

```python

def patchedAdaIn(x, s, b_patches):
    ''' x ~ b x c x h x w, the content (inpainted) image
        s ~ b x c x h x w, the style (conditioning) image
        b_patches ~ b x [ n_pathces x h x w ], binary masks denoting where to apply the style image
    '''
    
    # 1. Normalize the features
    y = F.instance_norm(x)
    eps = 1e-5
    
    # 2. Modulate the features according the the masks
    for batch_ix, patches in enumerate(b_patches):
        
        # 2.1 Get statistics to modulate the text patches 
        
        # Scale the masks the feature sizes (dims: n_patches x 1 x W x H)
        patches = interpolate(patches.unsqueeze(1), size=s.size()[2:]).detach() 
        
        # Extract the features corresponding to the masked regions (dims: n_patches x n_channels x W x H)
        t = s[batch_ix].unsqueeze(0) * patches 

        # Calculate the statistics (dims: n_patches x n_channels x 1 x 1)
        std  = (t.var([2, 3], keepdim=True) + eps).sqrt()
        mean = t.mean([2, 3], keepdim=True) 
        norm = patches.sum([2, 3], keepdim=True)

        # n_patches x n_channels x W x H
        mean = (patches * mean / ( norm + eps )).sum(0) 
        std  = (patches * std  / ( norm + eps )).sum(0)
        
        # 2.2 Get statistics to modulate the background
        
        # Extract the features corresponding to the background (dims: 1 x n_channels x 1 x 1) 
        bg_patches = 1 - patches.sum(0, keepdim=True)
        bg = s[batch_ix].unsqueeze(0) * bg_patches
    
        # Calculate the statistics (dims: n_patches x n_channels x 1 x 1)
        bg_std  = (bg.var([2, 3], keepdim=True) + eps).sqrt()
        bg_mean = bg.mean([2, 3], keepdim=True) 
        bg_norm = bg_patches.sum([2, 3], keepdim=True)
    
        mean += (bg_patches * bg_mean / ( bg_norm + eps )).squeeze(0) 
        std += (bg_patches * bg_std / ( bg_norm + eps )).squeeze(0) 
        
        # 2.3 Modulate the images
        y[batch_ix, ...] *= std
        y[batch_ix, ...] += mean 

    return y


```

# Directory structure

The code is shared between this directry and a [submodule](https://gitlab.com/arseny-tx/pytorch-cyclegan-and-pix2pix) which is based on the original pix2pix model. Here is a decriptin of the directory tree.

 - notebooks/ - jupyter notebooks
 - shared/
	- clusters.py : hierarchical bounding box clustering algorithm, allows to cluster nearby groups of bounding boxes
  	- mlt_dataset.py, coco_text.py, ubertext_dataset.py : text detection dataset parsers
  	- datasets.py, datasets_v2.py : data generators for training, they use the text detection dataset parsers to get data form datasets and create inputs for the generation networks
    	- datasets.py : uses the hierarchical clustering algorithm from clusters.py to cluster bouning boxes. The generated images contain such clusters.
    	- datasets_v2.py : by thresholding on the minimum bounding box edge select big enough bounding boxes.The generated images are created by taking neighbouring boxes.
  	- inpaint.py, text.py, masks.py : helpers to work with text inpainting
  	- perceptual_loss.py : perceptual losses.
  	- nbs_helpers.py, utils.py : misc helpers.
 - pytorch-cyclegan-and-pix2pix/ - slight tweaks to all parts of the code, added several models.
  	- models / c_networks, c_canny_model : the trained model.





