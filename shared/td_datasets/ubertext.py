from torch.utils import data
from PIL import Image
from pathlib import Path

import numpy as np

paths = {
    'train' : '/nfs/home/anerinovsky/data/tx/Uber-Text/train/',
    'val'   : '/nfs/home/anerinovsky/data/tx/Uber-Text/val',
    'test'  : '/nfs/home/anerinovsky/data/tx/Uber-Text/test',
}


class UberText(data.Dataset):

    def __init__(self, train=True, root=None, need_4k=False):

        if root is None:
            split = 'train' if train else 'test'
            root = paths[split] 
        
        root = Path(root)
        self.data = []

        
        def add_subdir(subdir):
            imgs = ( img for img in ( root/subdir).glob('*.jpg') if 'blurred' not in img.stem)
            data = ( (img, root/subdir/ ('truth_' + img.stem + '.txt')) for img in imgs )
            data = sorted(data, key=lambda x: x[0].stem)
            self.data.extend(data)

        add_subdir('1Kx1K')
        if need_4k:
            add_subdir('4Kx4K')

    def parse_gt_file(self, file):
        with open(file, 'r') as f:
            data = [ self.parse_gt_line(line) for line in f ]
        
        if data:
            polys, labels, langs, attrs = zip(*data)
            return np.array(polys), labels, langs, attrs
        print("No labels ", file)
        return np.array([]), [], [], []

    def parse_gt_line(self, line):
        # assert len(line) == 9, f'Error: label file contains more then 8 columns: \n {line}'
        line = line.split('\t')

        polygon = np.array(list(map(int, line[0].split(' ')))).reshape(-1, 2).tolist()
        lang = 'english'
        label = line[1]
        attr = line[2] 

        return polygon, label, lang, attr

    def __len__(self):
        return len(self.data)

    def __getitem__(self, ix):

        image_file, gt_file = self.data[ix]
        
        polys, labels, langs, attrs = self.parse_gt_file(gt_file)

        return image_file, polys, labels, langs, attrs