#!/bin/bash


is_unset() { bash -c "test -z \"\${$1}\"" ; }

export_if_unset() { 
	is_unset "$1" && { 
		export "$1=$2" 
		echo "[X] Setting $1 to $2"
	} || {
		echo "[V] $1 was set to ${!1}"
	}
}

export_if_unset EXP_NAME testrun
export_if_unset CUDA_VISIBLE_DEVICES 2
export_if_unset NUM_THREADS 8
export_if_unset BATCH_SIZE 16

WS_DIR=~/models/tx/pix2pix
EXP_DIR=$WS_DIR/$EXP_NAME


log_run() {
	mkdir -p $EXP_DIR
	{
		echo $run; date; echo;
		env
		echo '---------------------------------'
	} >> $EXP_DIR/runs.txt
}

load_weigths_once() {

	test ! -f $EXP_DIR/latest_net_G.pth  -o -n "$RESET_WS" && { 
		cp $WS_DIR/$1/latest_net_*.pth -t $EXP_DIR
		echo "Copying weights from $1"
		ls $EXP_DIR
	}
}

load_weigths_once_ep() {

	test ! -f $EXP_DIR/latest_net_G.pth  -o -n "$RESET_WS" && { 
		cp $WS_DIR/$1/${2}_net_D.pth $EXP_DIR/latest_net_D.pth
		cp $WS_DIR/$1/${2}_net_G.pth $EXP_DIR/latest_net_G.pth
		echo "Copying weights from $1"
		ls $EXP_DIR
	}
}


# export_if_unset RESET_WS 0
# export LC_ALL

export PYTHONPATH=..  

run=$1

pushd pytorch-CycleGAN-and-pix2pix > /dev/null
# dir pytorch-CycleGAN-and-pix2pix

	test $run = "def" && {
	# run def

		python train.py  --name $EXP_NAME \
			--model tx --dataset_mode mlt  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--batch_size 16 \
			--netG resnet_6blocks
			# --netD n_layers --n_layers_D 5 

		exit 0
	}

	test $run = "restart" && {

		EXP_NAME=bigger-net
		DISPLAY_ENV="bigger-net Сб ноя 30 22:25:06 MSK 2019"

		python train.py  --name $EXP_NAME \
			--model tx --dataset_mode mlt  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "$DISPLAY_ENV" \
			--batch_size 16 \
			--netG resnet_6blocks --continue_train
			# --netD n_layers --n_layers_D 5 

		exit 0
	}


	test $run = "canny" && {

		python train.py  --name $EXP_NAME \
		--model pix2pix --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--batch_size 16 \
			--netG resnet_6blocks
			# --netD n_layers --n_layers_D 5 

		exit 0
	}

	test $run = "canny_mask" && {

		python train.py  --name $EXP_NAME \
			--model canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--num_threads $NUM_THREADS \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--batch_size $BATCH_SIZE \
			--netG resnet_6blocks \
			--norm instance 
			# --netD n_layers --n_layers_D 5 

		exit 0
	}

	test $run = "c_canny" && {

		python train.py  --name $EXP_NAME \
		--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE  # --continue_train
			# --netD n_layers --n_layers_D 5 

		exit 0
	}



	test $run = "new_ds" && {

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5
			# --continue_train

		exit 0
	}


	test $run = "more_style" && {

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 #  --n_style_blocks 2  # --continue_train

		exit 0
	}


	test $run = "n_cs" && {

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --num_cs all #  --n_style_blocks 2  # --continue_train

		exit 0
		
	}

	test $run = "n_cs_last" && {

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_7blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --num_cs last #  --n_style_blocks 2  # --continue_train

		exit 0
		
	}


	test $run = "conv_prev_pp" && {

		log_run

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --lambda_tv 0.00001 --continue_train

		exit 0
		
	}



	test $run = "inpaint" && {

		log_run

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --inpaint_losses --continue_train

		exit 0
		
	}

	test $run = "noD" && {

		log_run

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --inpaint_losses --no_D

		exit 0
		
	}

	test $run = "noDstyle" && {

		log_run

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --inpaint_losses --no_D --lambda_style 520 --continue_train

		exit 0
		
	}


	test $run = "gLoss" && {

		log_run
		load_weigths_once smoth-tv-norm

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 \
			--smooth_labels --lambda_tv 0.0 --lambda_L1 10 --continue_train

		exit 0
		
	}

	test $run = "skip" && {

		log_run
		is_unset CONT || {
			echo Lading skip-modulate weights
			load_weigths_once skip-modulate
			APPEND_ARGS=--continue_train
		}

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --lambda_tv 0.00001  $APPEND_ARGS 

		exit 0
		
	}


	test $run = "skip_slim" && {

		log_run
	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --lambda_tv 0.00001  $APPEND_ARGS 

		exit 0
		
	}

	test $run = "skip_slim_inp" && {

		log_run
	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.01  $APPEND_ARGS 

		exit 0
		
	}


	test $run = "unet" && {

		log_run

		# Fails to inpaint 
		#
		#

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG cond_unet_256 \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.1  $APPEND_ARGS 

		exit 0
		
	}

	test $run = "skip_slim_inp_ds" && {

		log_run
	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --box_cluster_data_version 3 \
			--spectral_norm --smooth_labels --inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.05  --continue_train $APPEND_ARGS 

		exit 0
		
	}

	test $run = "new_instance" && {

		log_run
		is_unset CONT || {
			# echo Lading new_instance weights
			# load_weigths_once new_instance
			APPEND_ARGS=--continue_train
		}	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --box_cluster_data_version 35 --norm instance \
			--spectral_norm --smooth_labels --inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.05  $APPEND_ARGS 

		exit 0
		
	}

	test $run = "new_instance_cont" && {

		EXP_NAME='new_instance2'
		BATCH_SIZE=10
		START_EPOCH=6

		APPEND_ARGS="--continue_train --epoch_count $START_EPOCH"

		load_weigths_once new_instance

		log_run

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --box_cluster_data_version 35 --norm instance \
			--spectral_norm --smooth_labels --inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.05  $APPEND_ARGS 

		exit 0
		
	}



	test $run = "go" && {

		log_run
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		# 20 Mar:
		#	 removed --norm instance, EXP_NAME=batchnorm[fix]
		# Next: remove VGG losses, smaller discriminator
		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --box_cluster_data_version 35  \
			--spectral_norm --smooth_labels \
			--inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.05  #  $APPEND_ARGS 

		exit 0
		
	}

	test $run = "noise_style_image" && {

		log_run
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --box_cluster_data_version 35  \
			--spectral_norm --smooth_labels \
			--inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.05 \
			--noise_style_image #  $APPEND_ARGS 

		exit 0
		
	}

	test $run = "syth_train" && {

		log_run
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 \
			--spectral_norm --smooth_labels \
			--inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.5 \
			--box_dataset_name synthtext --tv_mask
			# --noise_style_image #  $APPEND_ARGS 
			# --box_cluster_data_version 35  \

		exit 0
		
	}

	test $run = "syth_train_cont" && {

		log_run
		load_weigths_once synthtrain
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 \
			--spectral_norm --smooth_labels \
			--inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.5 \
			--box_dataset_name synthtext --tv_mask \
			--save_by_iter \
			--continue_train --epoch_count 2
			# --noise_style_image #  $APPEND_ARGS 
			# --box_cluster_data_version 35  \

		exit 0
		
	}


	test $run = "syth_train_mlt" && {

		log_run
		load_weigths_once synthtrain2
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 \
			--spectral_norm --smooth_labels \
			--inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.5 \
			--box_dataset_name mlt --tv_mask \
			--save_by_iter \
			--continue_train --epoch_count 2
			# --noise_style_image #  $APPEND_ARGS 
			# --box_cluster_data_version 35  \

		exit 0
		
	}



	test $run = "morph_loss" && {
		echo HERE
		log_run
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 \
			--spectral_norm --smooth_labels --tv_mask \
			--inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.05 \
			--box_dataset_name v3.5
		exit 0
		
	}

	test $run = "morph_loss_cont" && {
		echo HERE
		log_run
		load_weigths_once morph_loss_old_ds
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 \
			--spectral_norm --smooth_labels --tv_mask \
			--inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.05 \
			--box_dataset_name v3.5 --continue_train --epoch_count 2
		exit 0
		
	}


	test $run = "morph_loss_no_D" && {
		echo HERE
		log_run
		# load_weigths_once morph_loss_old_ds
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 \
			--spectral_norm --smooth_labels --tv_mask \
			--inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.1 \
			--box_dataset_name v3.5  --no_D
		exit 0
		
	}



	test $run = "p_conv" && {
		
		log_run
		# load_weigths_once morph_loss_old_ds
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		python train.py  --name $EXP_NAME \
			--model p_conv --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--batch_size $BATCH_SIZE \
			--box_dataset_name v3.5 --net_cfg reg_dec #6skips
		exit 0
		
	}


	test $run = "p_conv_unmasked" && {
		
		log_run
		# load_weigths_once morph_loss_old_ds
		# is_unset CONT || {
		# 	# echo Lading new_instance weights
		# 	# load_weigths_once new_instance
		# 	# APPEND_ARGS=--continue_train
		# }	

		python train.py  --name $EXP_NAME \
			--model p_conv --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--batch_size $BATCH_SIZE \
			--box_dataset_name v3.5 \
			--net_cfg $NET_CFG --mask_as_separate_channel
		exit 0
		
	}


	test $run = "d_sched" && {

		log_run
		load_weigths_once d_sched_norm

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --lambda_tv 0.00001 --spectral_norm --continue_train # --D_schedule conservative

		exit 0
		
	}


	test $run = "d_sched_lr_dim" && {

		log_run
		load_weigths_once d_sched

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --lambda_tv 0.00001 --spectral_norm \
			--lr 0.00002\
			--continue_train --epoch_count 5 # --D_schedule conservative

		exit 0
		
	}

	test $run = "d_sched_lr_dim_slim" && {

		log_run
		# load_weigths_once d_sched

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --lambda_tv 0.00001 --spectral_norm 
			# --lr 0.00002
			# --continue_train --epoch_count 5 # --D_schedule conservative

		exit 0
		
	}

	test $run = "d_sched_lr_dim_slim_cont" && {

		log_run
		load_weigths_once_ep d_sched_lr_slim 5
		

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --lambda_tv 0.00001 --spectral_norm \
			--lr 0.00002 --continue_train --epoch_count 5 # --D_schedule conservative

		exit 0
		
	}


	test $run = "lr_sched" && {

		log_run
		# load_weigths_once d_sched

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG c_resnet_6blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5 --smooth_labels --lambda_tv 0.00001 --spectral_norm \
			--lr_policy step --lr_decay_iters 3


		exit 0
		
	}


	test $run = "selfattn" && {

		log_run
		# load_weigths_once d_sched

		python train.py  --name $EXP_NAME \
			--model c_canny --dataset_mode tx  --dataroot ~/data/tx --checkpoints_dir ~/models/tx/pix2pix \
			--no_flip --gan_mode lsgan --display_env "${EXP_NAME}_$(LC_ALL=C date)" \
			--num_threads $NUM_THREADS \
			--netG sc_resnet_3blocks \
			--batch_size $BATCH_SIZE \
			--netD n_layers --n_layers_D 5   \
			--spectral_norm --smooth_labels \
			--inpaint_losses --vgg2 --lambda_perc 10 --lambda_tv 0.05 \
			--selfattn_style --selfattn_cont


		exit 0
		
	}




popd > /dev/null



pushd pix2pix-tensorflow > /dev/null

	test $run = "coco_edges" && {

		python tools/dockrun.py python tools/process.py --input_dir ../SPADE/notebooks/coco_image_list.txt \
			--operation edges --output_dir /media/disk1/anerinovsky/data/tx/coco_edges

		exit 0
	}


popd > /dev/null

# bazel_docker(){
# 	docker run \
# 		  -e USER="$(id -u)" \
# 		  -u="$(id -u)" \
# 		  -v /src/workspace:$PWD \
# 		  -v /tmp/build_output:/tmp/build_output_${USER} \
# 		  -w /src/workspace \
# 		  --interactive --entrypoint=/bin/bash \
# 		  l.gcr.io/google/bazel:latest


# }


pushd multidim-image-augmentation > /dev/null
	

	test $run = "build-ops" && {

		mkdir -p /tmp/build_output_${USER}

		is_unset INT && {
			docker_args=""
			bazel_args="--output_user_root=/tmp/build_output test --python_version=py3 --config=nativeopt //..."
		} || {
			docker_args="--interactive --entrypoint=/bin/bash --rm --tty"
			bazel_args=""
		}


		docker run \
		  -e USER="$(id -u)" \
		  -u="$(id -u)" \
		  -v ${PWD}:/src/workspace \
		  -v /tmp/build_output_${USER}:/tmp/build_output \
		  -w /src/workspace \
		  $docker_args \
		  l.gcr.io/google/bazel:latest \
		  $bazel_args
		  
		  # test --python_version=py3 --config=nativeopt //...
		  # build --config=<asan/tsan/msan> -- //absl/... -//absl/types:variant_test
	}

popd > /dev/null


pushd crnn-pytorch > /dev/null

	test $run = "train-crnn-old" && {
		python train.py \
			--trainroot ~/data/tx/mnt/ramdisk/max/90kDICT32px/ --valroot ~/data/tx/mnt/ramdisk/max/90kDICT32px/ \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS

	}

	test $run = "train-crnn-jsynth" && {
		python train.py \
			--trainroot ~/data/tx/metrics/jssynth.lmdb \
			--valroot ~/data/tx/metrics/jssynth_val.lmdb \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS

	}

	test $run = "train-crnn-mlt" && {

		{
			echo ===========
			date
			echo ===========
		} >> crnn-mlt2.log

		python train.py \
			--trainroot ~/data/tx/metrics/mlt-train.lmdb --expr_dir ~/data/tx/crnn-models/crnn-mlt \
			--valroot ~/data/tx/metrics/mlt-val.lmdb \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS | tee -a crnn-mlt2.log

	}

	test $run = "train-crnn-syn" && {

		{
			echo ===========
			date
			echo ===========
		} >> crnn-syn.log

		python train.py \
			--trainroot ~/data/tx/metrics/syn-train.lmdb \
			--valroot ~/data/tx/metrics/mlt-val.lmdb \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS | tee -a crnn-syn.log

	}


	test $run = "train-crnn-mlt-syn" && {

		{
			echo ===========
			date
			echo ===========
		} >> crnn-mlt-syn.log

		python train.py \
			--trainroot ~/data/tx/metrics/mlt-syn-train.lmdb \
			--valroot ~/data/tx/metrics/mlt-val.lmdb --expr_dir ~/data/tx/crnn-models/crnn-mlt-syn \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS | tee -a crnn-mlt-syn.log

	}


	test $run = "train-crnn-mlt-syn-25p" && {

		{
			echo ===========
			date
			echo ===========
		} >> crnn-mlt-syn-25p.log

		python train.py \
			--trainroot ~/data/tx/metrics/mlt-syn-25p-train.lmdb \
			--valroot ~/data/tx/metrics/mlt-val.lmdb --expr_dir ~/data/tx/crnn-models/crnn-mlt-syn-25p \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS | tee -a crnn-mlt-syn-25p.log

	}

	test $run = "train-crnn-mlt-syn-2" && {

		{
			echo ===========
			date
			echo ===========
		} >> train-syn-mlt-crnn-2-3.log

		python train.py \
			--trainroot ~/data/tx/metrics/train-syn-mlt-crnn-2.lmdb \
			--valroot ~/data/tx/metrics/mlt-val.lmdb --expr_dir ~/data/tx/crnn-models/train-syn-mlt-crnn-2-3 \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS | tee -a train-syn-mlt-crnn-2-3.log

	}

	test $run = "val-crnn-mlt-coco" && {
		python train.py \
			--trainroot ~/data/tx/metrics/mlt-train.lmdb \
			--expr_dir ~/data/tx/crnn-models/_dummy \
			--valroot ~/data/tx/metrics/val-coco-crnn.lmdb \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS \
			--validate_model ~/data/tx/crnn-models/crnn-mlt/netCRNN_100_390.pth
	}


	test $run = "val-crnn-mlt-syn-2-coco" && {
		python train.py \
			--trainroot ~/data/tx/metrics/mlt-train.lmdb \
			--expr_dir ~/data/tx/crnn-models/_dummy \
			--valroot ~/data/tx/metrics/val-coco-crnn.lmdb \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS \
			--validate_model ~/data/tx/crnn-models/train-syn-mlt-crnn-2/netCRNN_last.pth
	}



	test $run = "val-crnn-mlt-mlt" && {
		python train.py \
			--trainroot ~/data/tx/metrics/mlt-train.lmdb \
			--expr_dir ~/data/tx/crnn-models/_dummy \
			--valroot ~/data/tx/metrics/mlt-val.lmdb \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS \
			--validate_model ~/data/tx/crnn-models/crnn-mlt/netCRNN_200_390.pth
	}

	test $run = "val-crnn-mlt-syn" && {
		python train.py \
			--trainroot ~/data/tx/metrics/mlt-train.lmdb \
			--expr_dir ~/data/tx/crnn-models/_dummy \
			--valroot ~/data/tx/metrics/val-syn-crnn.lmdb \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS \
			--validate_model ~/data/tx/crnn-models/crnn-mlt/netCRNN_200_390.pth
	}


	test $run = "val-crnn-syn-mlt" && {
		python train.py \
			--trainroot ~/data/tx/metrics/mlt-train.lmdb \
			--expr_dir ~/data/tx/crnn-models/_dummy \
			--valroot ~/data/tx/metrics/mlt-val.lmdb \
			--batch_size $BATCH_SIZE --num_workers $NUM_THREADS \
			--validate_model ~/data/tx/crnn-models/crnn-mlt-syn/netCRNN_last.pth
	}

popd > /dev/null




# ^CTraceback (most recent call last):
#   File "train.py", line 50, in <module>
#     for i, data in enumerate(dataset):  # inner loop within one epoch
#   File "/nfs/home/anerinovsky/tx/pytorch-CycleGAN-and-pix2pix/data/__init__.py", line 105, in __iter__
#     i, data = next(it)
#   File "/media/disk1/anerinovsky/anaconda3/lib/python3.7/site-packages/torch/utils/data/dataloader.py", line 345, in __next__
#     data = self._next_data()
#   File "/media/disk1/anerinovsky/anaconda3/lib/python3.7/site-packages/torch/utils/data/dataloader.py", line 841, in _next_data
#     idx, data = self._get_data()
#   File "/media/disk1/anerinovsky/anaconda3/lib/python3.7/site-packages/torch/utils/data/dataloader.py", line 808, in _get_data
#     success, data = self._try_get_data()
#   File "/media/disk1/anerinovsky/anaconda3/lib/python3.7/site-packages/torch/utils/data/dataloader.py", line 761, in _try_get_data
#     data = self._data_queue.get(timeout=timeout)
#   File "/media/disk1/anerinovsky/anaconda3/lib/python3.7/multiprocessing/queues.py", line 104, in get
#     if not self._poll(timeout):
#   File "/media/disk1/anerinovsky/anaconda3/lib/python3.7/multiprocessing/connection.py", line 257, in poll
#     return self._poll(timeout)
#   File "/media/disk1/anerinovsky/anaconda3/lib/python3.7/multiprocessing/connection.py", line 414, in _poll
#     r = wait([self], timeout)
#   File "/media/disk1/anerinovsky/anaconda3/lib/python3.7/multiprocessing/connection.py", line 920, in wait
#     ready = selector.select(timeout)
#   File "/media/disk1/anerinovsky/anaconda3/lib/python3.7/selectors.py", line 415, in select
#     fd_event_list = self._selector.poll(timeout)
# KeyboardInterrupt