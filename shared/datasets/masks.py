from ..utils import np_to_shapely, shapely_to_np
import numpy as np

from shapely.geometry import box, Polygon

import cv2

def mask_from_poly(img_size, poly_np):

    # assert poly_np.size > 0 
    # assert all( poly_np >= 0 )


    img_w, img_h = img_size
    mask = np.zeros( (img_h, img_w) )

    # poly_sh = np_to_shapely(poly_np)
    # TODO: quite sloppy here
    # poly_sh = poly_sh.intersection(box(0, 0, img_w, img_h))
    # if not isinstance(poly_sh, Polygon):
    #     return mask.astype(np.bool)
    # poly_np = shapely_to_np(poly_sh)

    if poly_np.size == 0:
        return mask.astype(np.bool)

    poly_np = np.r_[poly_np, poly_np[:1]]

    cv2.fillConvexPoly(mask, poly_np.astype(np.int32), 1)
    
    return mask.astype(np.bool)


def bg_img_mask(img_size, polys_np, rectify_poly=True):
    img_w, img_h = img_size
    bg_mask = np.zeros((img_h, img_w), np.bool)
    masks = []

    for poly_np in polys_np:

        if rectify_poly:
            try:
                poly_sh = np_to_shapely(poly_np)
                poly_np = shapely_to_np(poly_sh.minimum_rotated_rectangle)
            except Exception as e:
                print("Failed to porecess poly with exception", e)


        mask = mask_from_poly(img_size, poly_np)
        masks.append(mask)
        bg_mask |= mask
        
    return 1 - bg_mask, masks