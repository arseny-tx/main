

from .clusters import scale_to_square, cluster_polys
from ..utils import np_to_shapely, shapely_to_np
from .masks import bg_img_mask

from pathlib import Path
from skimage import feature

from torch.utils import data
from joblib import dump, load

import numpy as np

def fill_polys_with_edges(img, polys_np, sigma=.1):
    bg_mask = bg_img_mask(img.size, polys_np)

    bw_img = np.array(img.convert('L'))
    edges = feature.canny(bw_img, sigma=sigma) 
    
    text_edges = (1-edges) * (1-bg_mask)

    text_edges = np.repeat(text_edges[None, ...], 3, axis=0) * 255
    
    
    img = np.array(img).transpose(2, 0, 1) * bg_mask + text_edges
    
    return Image.fromarray(img.transpose(1, 2, 0).astype('uint8'))

# def image_with_edges(img, sigma=.1):


def sample_from_cluster(img, clusters, cluster_ix, sigma=.1):

    polys_np = list(it.chain.from_iterable(clusters))

    bg_mask, masks = bg_img_mask(img.size, polys_np)

    bw_img = np.array(img.convert('L'))
    text_edges = feature.canny(bw_img, sigma=sigma) 
    text_edges = (1-text_edges) * (1-bg_mask)
    text_edges = np.repeat(text_edges[None, ...], 3, axis=0) * 255
    
    img_with_edges = np.array(img).transpose(2, 0, 1) * bg_mask + text_edges
    
    img_with_edges = Image.fromarray(img_with_edges.transpose(1, 2, 0).astype('uint8'))

    arr2im = lambda img: Image.fromarray(img.astype('uint8')*255)

    img_mask = arr2im(bg_mask)
    
    # img_with_edges = fill_polys_with_edges(img, polys_np, sigma=.1)
    
    cc = np_cluster_crop(clusters[cluster_ix])
    int_box = cc.intersection(box(0, 0, img.width, img.height)).bounds

    masks = [ arr2im(m).crop(box=int_box) for m in masks ]
    return img_with_edges.crop(box=int_box), img.crop(box=int_box), img_mask.crop(box=int_box), masks




def _create_cluster_lookup(images_with_clusters):
    cluster_lookup = []
    for img_path, clusters in images_with_clusters.items():
        for cluster_ix, cluster in enumerate(clusters):
            
            cc = np_cluster_crop(cluster)

            minx, miny, maxx, maxy = cc.bounds
            
            if maxx - minx >= 80:
                cluster_lookup.append((img_path, cluster_ix))

    return cluster_lookup



class BoxClustersDataset(data.Dataset):
    def __init__(self):

        base_dir = Path('~/data/tx').expanduser()
        file = base_dir / 'images_with_clusters_better_clusters'
        images_with_clusters = load(file)
        print('Loaded images_with_clusters')

        file = base_dir / 'cluster_lookup'
        cluster_lookup = load(file)
        print('Loaded cluster_lookup')
        
        self.cluster_lookup = cluster_lookup
        self.images_with_clusters = images_with_clusters



    def __getitem__(self, ix):

        img_path, cluster_ix = self.cluster_lookup[ix]

        clusters = self.images_with_clusters[img_path]

        img = Image.open(img_path).convert('RGB')
        return (*sample_from_cluster(img, clusters, cluster_ix), img_path)

    def __len__(self):
        return len(self.cluster_lookup)


