from .v2 import \
	combine_edges_and_bg, feature, np, \
	Image, np_to_shapely, poly_to_crop



def compute_crop_and_cluster(ix, main_boxes, boxes_lookup, intersect_with_crop=True, border_size=60):

    img_path, (image_w, image_h), poly_py, poly_id = main_boxes[ix]

    poly_sh = np_to_shapely(np.array(poly_py))
    polys_sh = [np_to_shapely(np.array(poly_py)) for poly_py in boxes_lookup[img_path]]


    crop = poly_to_crop(poly_sh, image_w, image_h, border_size=border_size)
            
    cluster = assamble_cluster(crop, poly_sh, poly_id, polys_sh)

    return img_path, crop, cluster





def create_image_with_edges(img, bg_img, bg_mask, sigma=.5):
	
    text_edges = feature.canny(np.array(img.convert('L'))/255, sigma=sigma) 
    # text_edges = closing(text_edges, square(2))
    # text_edges = remove_small_objects(text_edges, 10, connectivity=5)

    img_with_edges = combine_edges_and_bg(text_edges, np.array(bg_img)/255, bg_mask)

    print(img_with_edges.shape)

    img_with_edges = Image.fromarray(img_with_edges.astype('uint8'))

    return img_with_edges




def create_image_with_edges_and_masks(img, polys_np, fail_on_bad_crop=False, edges_img=None):

    bg_mask, masks = bg_img_mask(img.size, polys_np, rectify_poly=False)

    img = adjust_buggy_crop(img, *bg_mask.shape, fail_on_bad_crop=fail_on_bad_crop)
    

    if edges_img is not None:
        edges_img = feature.canny(np.array(edges_img.convert('L'))/255, sigma=.5) 
    else:
        edges_img = feature.canny(np.array(img.convert('L'))/255, sigma=.5) 

    img_with_edges = combine_edges_and_bg(edges_img, bg_img, bg_mask)


    arr2im = lambda img: Image.fromarray(img.astype('uint8')*255)

    img_with_edges = arr2im(img_with_edges)
    img_mask = arr2im(bg_mask)
    masks = [ arr2im(m) for m in masks ]

    return img_with_edges, img_mask, masks





def assamble_sample(img_path, crop, cluster, edges_img_path=None):
    

    cluster = adjust_cluster_to_crop(crop, cluster)

    # minx, miny, maxx, maxy = map(int, crop.bounds)
    # crop_w, crop_h = (maxx - minx), (maxy - miny)

    c0 = []
    for p in cluster:
        if isinstance(p, Polygon):
            c0.append(shapely_to_np(p))
        else:
            print(f'{p} in cluster')
    cluster = c0

    img = Image.open(img_path).convert('RGB').crop(box=crop.bounds)
    
    if edges_img_path is not None:
        edges_img = Image.open(edges_img_path).convert('RGB').crop(box=crop.bounds)
    else:
        edges_img = None
    

    img_with_edges, bg_mask, masks = create_image_with_edges_and_masks(img, cluster, edges_img=edges_img)

    return img_with_edges, img, bg_mask, masks