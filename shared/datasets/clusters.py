from shapely.geometry import Polygon, MultiPolygon, box
from shapely.affinity import scale, translate

from PIL import Image

import itertools as it

import numpy as np

from ..utils import np_to_shapely, shapely_to_np



def scale_to_square(c_env, env_scale, interpret_scale_as_pixes=False, need_square=True):

    minx, miny, maxx, maxy = c_env.bounds
    w, h = maxx - minx, maxy - miny

    if interpret_scale_as_pixes:

        xfact, yfact = (w+env_scale) / w, (h+env_scale) / h

        if need_square:
            if w > h:
                yfact = (w+env_scale) / h
            else:
                xfact = (h+env_scale) / w

    else:
        if need_square:
            xfact, yfact = (1, w/h) if w > h else (h/w, 1)

        xfact, yfact = xfact*env_scale, yfact*env_scale

    return scale(c_env, xfact, yfact)




def cluster_polys(polys_sh, use_rects=True, **scale_args):
    
    polys_sh = np.array(polys_sh)
    if use_rects:
        rects_sh = np.array([p.minimum_rotated_rectangle for p in polys_sh])
    
    
    clusters = np.arange(len(polys_sh))
    
    def cluster_poly(c):
        x = rects_sh[clusters == c] if use_rects else polys_sh[clusters == c]   
        return MultiPolygon(x.tolist())
    
    def cluster_envelope(c): 
        env = cluster_poly(c).envelope
        sc_env = scale_to_square(env, **scale_args)
        return sc_env
    
    while True:
        
        queue = list(np.unique(clusters))
        n_old_clusters, n_new_clusters = len(queue), 0
        
        while queue:
            c = queue.pop()
            env = cluster_envelope(c)
            for c0 in np.unique(clusters):
                
                if c0 == c:
                    continue
                
                c0_poly = cluster_poly(c0)
                if env.intersects(c0_poly):
                    clusters[clusters==c0] = c
                    env = cluster_envelope(c)
                    if c0 in queue:
                        queue.remove(c0)
            
            n_new_clusters += 1
        if n_old_clusters == n_new_clusters:
            break
        
    res = []
    for c in np.unique(clusters):
        res.append(polys_sh[clusters==c].tolist())
    return res

def cluster_polys_np(polys_np, **kw):
    polys_sh = list(map(np_to_shapely, polys_np))
    clusters = cluster_polys(polys_sh, **kw)
    
    return [ list(map(shapely_to_np, c)) for c in clusters ]

def np_to_sh_cluster(cluster):
    return MultiPolygon(list(map(np_to_shapely, cluster)))

def sh_cluster_crop(cluster):
    return scale_to_square(cluster.envelope, 1.6)

def np_cluster_crop(cluster):
    return sh_cluster_crop(np_to_sh_cluster(cluster))




