from torch.utils import data
from PIL import Image
from pathlib import Path

import numpy as np
import scipy.io

class SynthText(data.Dataset):
    """
        Links:
            gt.mat file: http://www.robots.ox.ac.uk/~vgg/data/scenetext/readme.txt
            dataset: http://www.robots.ox.ac.uk/~vgg/data/scenetext/
            tool: https://github.com/ankush-me/SynthText    
    """

    def __init__(self, root=None):
        if root is None:
            root = Path('~/datasets/archives/SynthText').expanduser()        

        self.root = Path(root)
        self.data = scipy.io.loadmat(root/'gt.mat')

        
    def __len__(self):
        return len(self.data['imnames'][0])

    def __getitem__(self, ix):

        image_file, polys_np, labels = self.data['imnames'][0][ix], self.data['wordBB'][0][ix], self.data['txt'][0][ix]

        if polys_np.ndim == 2:
            polys_np = polys_np[:, :, None]

        assert polys_np.shape[:-1] == (2, 4), f'Wired polys_np in {ix} -- {polys_np.shape} expected (2, 4, *)'

        image_file, polys_np, labels = self.root / image_file[0], polys_np.transpose(2, 1, 0), list(labels)

        n_polys = len(polys_np)

        return image_file, polys_np, labels + ['text']*(n_polys - len(labels)), ['english']*n_polys, [{}]*n_polys


from joblib import dump, load
class SynthTextClean(data.Dataset):
    """
        Links:
            gt.mat file: http://www.robots.ox.ac.uk/~vgg/data/scenetext/readme.txt
            dataset: http://www.robots.ox.ac.uk/~vgg/data/scenetext/
            tool: https://github.com/ankush-me/SynthText    
    """

    def __init__(self, root):

        self.root = Path(root)
        self.data = load(root/'data.pkl')

        self.ixs = sorted(self.data.keys())

    def __len__(self):
        return len(self.data)

    def __getitem__(self, ix):

        image_file = self.ixs[ix]

        polys_np, labels = self.data[image_file]['wordBB'], self.data[image_file]['txt']
        if polys_np.ndim == 2:
            polys_np = polys_np[:, :, None]


        img_paths = { 
            'orig' : str(self.root/'imgs-orig'/image_file), 
            'wbg'  : str(self.root/ 'imgs-wbg'/image_file),
        }

        assert polys_np.shape[:-1] == (2, 4), f'Wired polys_np in {ix} -- {polys_np.shape} expected (2, 4, *)'

        polys_np, labels = polys_np.transpose(2, 1, 0), list(labels)

        n_polys = len(polys_np)

        return img_paths, polys_np, labels + ['text']*(n_polys - len(labels)), ['english']*n_polys, [{}]*n_polys




if __name__ == '__main__':
    pass





        