from pathlib import Path
import numpy as np

from ..utils import np_to_shapely, shapely_to_np
from .clusters import scale_to_square
from .masks import bg_img_mask

from torch.utils import data
from joblib import dump, load

from shapely.affinity import scale, translate

from skimage import feature
from PIL import Image
from shapely.geometry import Polygon

def adjust_cluster_to_crop(crop, cluster):
    minx, miny, maxx, maxy = map(int, crop.bounds)
    return [ translate(poly_sh, -minx, -miny) for poly_sh in cluster]


def adjust_crop_to_image(crop, image_w, image_h):
    minx, miny, maxx, maxy = crop.bounds
    
    if minx < 0: crop = translate(crop, xoff=-minx)
    if miny < 0: crop = translate(crop, yoff=-miny)
    
    if maxx > image_w: crop = translate(crop, xoff=-(maxx - image_w))
    if maxy > image_h: crop = translate(crop, yoff=-(maxy - image_h))
    
    return crop


def poly_to_crop(poly_sh, image_w, image_h, border_size=60 ):

    crop = scale_to_square(poly_sh.envelope, border_size, interpret_scale_as_pixes=True)
    crop = adjust_crop_to_image(crop, image_w, image_h)

    return crop

def assamble_cluster(crop, crop_poly_sh, crop_poly_id, polys_sh):
    polys_sh = [ 
        # np_to_shapely(np.array(poly_py))
        poly_sh
             for poly_id, poly_sh in enumerate(polys_sh)
                if crop_poly_id != poly_id
    ]

    cluster = [crop_poly_sh.minimum_rotated_rectangle]

    for p in polys_sh:
        if p.intersects(crop):
            p = p.minimum_rotated_rectangle
            p = p.intersection(crop)
            if not isinstance(p, Polygon):
                print("Not polygon created by intersection")
            else:
                cluster.append(p)

    return cluster



from skimage.morphology import remove_small_objects, closing, square



def combine_edges_and_bg(edges_img, bg_img, bg_mask):
    
    # n_px = 1 - text_edges.sum()/(1-bg_mask).sum()
    # print(n_px)

    edges_img = (1-edges_img) * (1 - bg_mask)
    edges_img = np.repeat(edges_img[None, ...], 3, axis=0) * 255

    # IDK !
    return ((bg_img/255).transpose(2, 0, 1) * bg_mask + edges_img*255).transpose(1, 2, 0)



def adjust_buggy_crop(img, crop_heigth, crop_width, max_offset = 5, fail_on_bad_crop=False):

    # hmm...
    dy, dx = crop_heigth - img.height, crop_width - img.width

    message = f"cropped image size don't expectaions: crop_height - img.height = {dx} crop_width - img.width = {dy}"

    if fail_on_bad_crop and abs(dy) + abs(dx) > 0:
        raise ValueError(message)

    if abs(dy) <= max_offset and abs(dx) < max_offset:
        img = img.resize((crop_width, crop_heigth))
    elif abs(dy) > max_offset or abs(dx) > max_offset:
        raise ValueError(message)

    return img

from skimage import feature
from skimage.morphology import dilation, square



from tqdm.autonotebook import tqdm
from skimage.morphology import square, dilation, binary_opening, binary_erosion, binary_closing

from ..utils import elastic_transform

class BoxClustersDatasetV2(data.Dataset):
    def __init__(self, name, border_size):

        base_dir = Path('~/data/tx').expanduser()

        self.main_boxes = load(base_dir / f'clusters_{name}.main_boxes')

        # 35
        if (base_dir / f'clusters_{name}.boxes_lookup.shard.0').exists():
            shards = base_dir.glob(f'clusters_{name}.boxes_lookup.shard.*')            
            shards = [load(shard) for shard in tqdm(list(shards))]

            self.boxes_lookup = dict(sum( (list(s) for s in shards), []))            
        else:
            self.boxes_lookup = load(base_dir / f'clusters_{name}.boxes_lookup')

            
        self.border_size = border_size
            
        # if version == 2:
        #     self.border_size = 60  
        # else: 
        #     self.border_size = 80  

    def __getitem__(self, ix):

        # try:
        img_with_edges, img, img_mask, masks, patch_edges, img_path, crop = \
            self.getitem_unsafe(ix)
        # except:
        #     print(f'Failed to get data for index {ix}')
        #     raise
        
        return img_with_edges, img, img_mask, masks, patch_edges, img_path, crop

    def load_data(self, ix):
        img_paths, (image_w, image_h), main_poly_py, main_poly_id = self.main_boxes[ix]

        main_poly_sh = np_to_shapely(np.array(main_poly_py))
        

        if not isinstance(img_paths, dict):
            img_paths = { 'img_path' : img_paths }

        polys_sh = [np_to_shapely(np.array(poly_py)) for poly_py in self.boxes_lookup[img_paths['img_path']]]

        return img_paths, (image_w, image_h), main_poly_sh, main_poly_id, polys_sh

    def getitem_unsafe(self, ix):
        

        img_paths, (image_w, image_h), main_poly_sh, main_poly_id, polys_sh = \
            self.load_data(ix)

        crop = poly_to_crop(main_poly_sh, image_w, image_h, border_size=self.border_size)
                
        cluster = assamble_cluster(crop, main_poly_sh, main_poly_id, polys_sh)

        cluster = adjust_cluster_to_crop(crop, cluster)


        img = Image.open(img_paths['img_path'])
        img = img.convert('RGB').crop(box=crop.bounds)

        cluster = [shapely_to_np(poly_sh) for poly_sh in cluster]

        bg_mask, masks = bg_img_mask(img.size, cluster, rectify_poly=False)
        # bg_mask = 1-bg_mask

        img = adjust_buggy_crop(img, *bg_mask.shape, fail_on_bad_crop=False)

        if 'edges_img_path' is img_paths:
            
            edges_img = Image.open(img_paths['edges_img_path'])
            edges_img = edges_img.convert('RGB').crop(box=crop.bounds)

            edges_img = feature.canny(np.array(edges_img.convert('L'))/255, sigma=0) 
        else:

            edges_img = feature.canny(np.array(img.convert('L'))/255, sigma=.5) 


        
        img_with_edges = combine_edges_and_bg(edges_img, np.array(img), bg_mask)

        #
        # There is a bug with scaling, that's why img*255
        #
        np_to_pil = lambda img: Image.fromarray((img*255).astype('uint8'))

        img_with_edges, bg_mask = np_to_pil(img_with_edges), np_to_pil(bg_mask)
        masks = [ np_to_pil(m) for m in masks ]

        patch_edges = self.tv_mask(img_with_edges, bg_mask)


        return img_with_edges, img, bg_mask, masks, patch_edges, img_paths['img_path'], crop



    
    def noise_style_image(self, img, sigma_coef=5):
        img = np.array(img)
        img = elastic_transform(img, img.shape[1]*sigma_coef, img.shape[1]*0.08, img.shape[1]*0.008)
        return Image.fromarray(img)

    def tv_mask(self, src_img, bg_mask):
        
        src_img, bg_mask = np.array(src_img), np.array(bg_mask)[:, :, np.newaxis]         

        tv_mask = ~(np.array(src_img) & ~bg_mask  | bg_mask)[:, :, 0]
        tv_mask = binary_erosion(dilation(tv_mask, square(17)) ^ tv_mask, square(3))
        tv_mask |= dilation(bg_mask[:, :, 0] & ~binary_erosion(bg_mask[:, :, 0], square(7)), square(3)).astype(bool)

        # print('>>>>', tv_mask.shape)
        return Image.fromarray(tv_mask)

    def __len__(self):
        return len(self.main_boxes)



