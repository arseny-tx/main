from torch.utils import data
from PIL import Image
from pathlib import Path

import numpy as np


paths = {
    'test' : '/media/disk1/anerinovsky/data/tx/mlt-val',
    'train' : '/media/disk1/anerinovsky/data/tx/',
}

class ICDAR_MLT(data.Dataset):

    LANGS = {
        'Arabic' : (1, 1000),
        'English' : (1001, 2000),
        'French' : (2001, 3000),
        'Chinese' : (3001, 4000),
        'German' : (4001, 5000),
        'Korean' : (5001, 6000),
        'Japanese' : (6001, 7000),
        'Italian' : (7001, 8000),
        'Bangla' : (8001, 9000),
        'Hindi' : (9001, 10000)
    }

    @classmethod
    def get_lang_ixs(cls, lang):
        b, e  = cls.LANGS[lang]
        return list(range(b-1, e))

    def __init__(self, train=True, root=None):

        if root is None:
            split = 'train' if train else 'test'
            root = paths[split]
            
        root = Path(root)

        img_dir, gt_dir = root / 'images', root / 'gt'
        print('gt_dir ', gt_dir)

        def guess_file(stem):
            for ext in ['.jpg', '.png', '.gif']:

                file = img_dir / ( stem + ext )
                if file.exists():
                    return file

                file = img_dir / ( stem.lstrip('gt_') + ext )
                if file.exists():
                    return file                
            assert False, f"No file for {stem} in {img_dir} from {gt_dir} found"

        data = (( guess_file(gt.stem) , gt) 
                        for gt in gt_dir.iterdir()  )
        data = sorted(data, key=lambda x: int(x[1].stem.split('_')[2]) )
        self.data = list(data)

    def parse_gt_file(self, file):
        with open(file, 'r') as f:
            data = [ self.parse_gt_line(line) for line in f ]
        
        if data:
            polys, labels, langs = zip(*data)
            return np.array(polys), labels, langs
        print("No labels ", file)
        return np.array([]), [], []

    def parse_gt_line(self, line):
        #assert len(line) == 9, f'Error: label file contains more then 8 columns: \n {line}'
        line = line.split(',')
        line = [i.strip('\ufeff').strip('\xef\xbb\xbf') for i in line] 

        x1, y1, x2, y2, x3, y3, x4, y4 = map(int, line[:8])
        lang = line[8]
        label = None if line[9] == '###' else (','.join(line[9:])).rstrip('\n')

        polygon = [[x1, y1], [x2, y2], [x3, y3], [x4, y4]]

        return polygon, label, lang

    def __len__(self):
        return len(self.data)

    def __getitem__(self, ix):

        image_file, gt_file = self.data[ix]
        
        polys, labels, langs = self.parse_gt_file(gt_file)

        return image_file, polys, labels, langs