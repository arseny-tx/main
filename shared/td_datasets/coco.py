from .coco_text import COCO_Text

from torch.utils import data
import numpy as np

from PIL import Image
from pathlib import Path


COCO_ROOT = Path('~/datasets/huawei/COCO').expanduser()

class COCO(data.Dataset):

    @staticmethod
    def create_pkg(ann=None):
        if ann is None:
            root = COCO_ROOT            
            ann = root / 'cocotext.v2/cocotext.v2.json'
            # ann = { 
            #     'v2'   : root / 'cocotext.v2/cocotext.v2.json', 
            #     'v1.7' : root / 'COCO_Text/COCO_Text.json'  
            # }[version]
        return COCO_Text(ann)


    def __init__(self, train=True, pkg=None, ann=None, 
                    img_dir=None, boxes_type='poly'):
        
        if pkg is None:
            pkg = COCO.create_pkg(ann)

        if img_dir is None:
            root =  COCO_ROOT # config.dataset_paths['COCO']            
            img_dir =  root / 'train2014'

        self.img_dir = img_dir
        self.coco = pkg

        if train:
            self.ids = self.coco.train
        else:
            self.ids = self.coco.val

        self.ids = [ id for id in self.ids if self.coco.imgToAnns[id] ]
        self.boxes_type = boxes_type

    def __getitem__(self, ix):

        img_id = self.ids[ix]        
        img_t = self.coco.loadImgs(img_id)
        assert len(img_t) == 1, f"More then one image returned for {img_id}"
        img_t = img_t[0]

        img_file = self.img_dir / img_t['file_name']

        ann_id = self.coco.imgToAnns[self.ids[ix]]
        anns_t = self.coco.loadAnns(ann_id)

        if anns_t:
            polys_np, labels, attrs, langs = \
                zip(*(self._parse_ann(ann) for ann in anns_t))
        else:
            polys_np, labels, attrs, langs = [],[],[]

        
        return img_file, polys_np, labels, langs, attrs

    def index(self, ix):

        return self[ix][:-1]

    def _parse_ann(self, ann):

        if self.boxes_type == 'poly':
            poly = np.array(ann['mask'])
            poly_np = np.stack([poly[0::2], poly[1::2]]).T
            poly_np = np.flip(poly_np, axis=0)
            poly_np = np.roll(poly_np, 1, axis=0)
        elif self.boxes_type == 'rect':
            x, y, w, h = ann['bbox']
            poly_np = np.array([[x, y],[x+w, y],[x+w, y+h],[x, y+h]])
        else:
            raise ValueError(f'Invlid boxes_type {self.boxes_type}')

        return poly_np, ann['utf8_string'], ann['language'], {
            'legibility' : ann['legibility'], 
            'class' : ann['class']
        }
    def __len__(self):
        return len(self.ids)
