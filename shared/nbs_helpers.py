import sys

if '../pytorch-CycleGAN-and-pix2pix' not in sys.path:
    sys.path.insert(0, '../pytorch-CycleGAN-and-pix2pix')

from data.base_dataset import get_params, get_transform
from options.train_options import TrainOptions
from torchvision.transforms.functional import to_tensor, to_pil_image

import argparse




from .utils import np_to_shapely, shapely_to_np

from options.test_options import TestOptions
from models import create_model
from util.util import tensor2im

import argparse
import os
from pathlib import Path
import numpy as np








# def build_opt(exp_name='cond-ds-v2', epoch=None, model='c_canny', netG='c_resnet_6blocks'):
def build_opt(**kw):
    parser = argparse.ArgumentParser()

    opts = TestOptions()
    opts.initialize(parser)
    opt, _ = parser.parse_known_args(['--dataroot', '.'])

    # hard-code some parameters for test
    opt.num_threads = 0   # test code only supports num_threads = 1
    opt.batch_size = 1    # test code only supports batch_size = 1
    opt.serial_batches = True  # disable data shuffling; comment this line if results on randomly chosen images are needed.
    opt.no_flip = True    # no flip; comment this line if results on flipped images are needed.
    opt.display_id = -1 
    opt.isTrain = False
    opt.gpu_ids = [0] # []'cuda:2'

    opt.inpaint_losses = False

    opt.model_suffix = ''
    opt.name = 'cond-ds-v2'

    opt.n_style_blocks = 1
    opt.model = 'c_canny' # 'c_canny'
    opt.num_cs = 'last'


    opt.netG = 'c_resnet_6blocks'
    opt.norm = 'batch'
    opt.checkpoints_dir = str(Path('~/models/tx/pix2pix').expanduser())
    opt.num_patches = 8
    opt.mask_as_separate_channel = False


    opt.no_D = True
    opt.num_threads = 0
    opt.dataset_mode = 'tx'
    opt.batch_size = 12
    opt.serial_batches = False
    opt.preprocess = 'resize'
    opt.noise_style_image = False
    opt.modulate_bg_mode = 'normal'
    opt.D_schedule = 'default'

    #
    # Dataset ops
    #

    opt.dataset_mode = 'tx'
    opt.box_dataset_name = 'v3.5'
    opt.box_cluster_dataset_version = 2
    opt.border_size = 80
    opt.num_patches = 16
    opt.noise_style_image = False


    for k, v in kw.items():
        setattr(opt, k, v)


    return opt

from data.tx_dataset import TxDataset
from data import CustomDatasetDataLoader

def build_dataset(opt=None):
    if opt is None:
        opt = build_opt()
    
    return TxDataset(opt)


def build_dataloader(dataset = None, opt=None):
    if opt is None:
        opt = build_opt()

    if dataset is None:
        dataset = TxDataset(opt)

    return CustomDatasetDataLoader(opt=opt, dataset=dataset)


def build_model(gpu=3, opt=None):

    os.environ['CUDA_VISIBLE_DEVICES'] = str(gpu)
        
    if opt is None:
        opt = build_opt()

    model = create_model(opt)
    model.setup(opt)
    model.eval()
    
    return model


from shapely.geometry import Polygon




def draw_polys(polys, ax, add_numbers=False, rectify=False, color='red'):
    
    from matplotlib.patches import Polygon as MplPolygon, Circle as MplCircle
    
    to_rect = lambda p: shapely_to_np(np_to_shapely(p).minimum_rotated_rectangle)
    

    def draw_poly(poly_np, ax, name):
        if rectify:
            poly_np = to_rect(poly_np)
        P = MplPolygon(poly_np, edgecolor=color, linewidth=2, fill=None)
        ax.add_patch(P)  

        if add_numbers:
        # for ix, point in enumerate(poly_np):
            ax.text(*poly_np[0], name, color='blue')

    for i, p in enumerate(polys):
        if isinstance(p, Polygon):
            p = shapely_to_np(p)
        draw_poly(p, ax, str(i))
        
    
from .datasets.clusters import scale_to_square

def draw_clusters(clusters, env_scale=1.6, **scaler_args):
    for cluster, color in zip(clusters, it.cycle(['red', 'blue', 'pink', 'green', 'yellow'])):
        cluster = list(map(np_to_shapely,cluster))

        draw_polys([p.minimum_rotated_rectangle for p in cluster], plt.gca(), color)
        # draw_polys(cluster, plt.gca(), color)


        c_env = MultiPolygon(cluster).envelope
        draw_polys([scale_to_square(c_env, env_scale, **scaler_args)], plt.gca(), 'orange')
        # print(cluster, MultiPolygon(cluster).envelope)
        
        
from multiprocessing import Pool, Value
from tqdm import tqdm_notebook as tqdm

class InParallelWithBar:
    
    def __init__(self, total, n_workers=10):
        self.n_workers = n_workers
        self.total = total
        self.counter = Value('i', 0)
        
    def run(self, worker):
        with Pool(processes=self.n_workers) as pool:
        
            res = pool.map_async(worker, range(self.n_workers))

            bar = tqdm(total = self.total)

            while not res.ready():    
                res.wait(1)

                with self.counter.get_lock():
                    self.counter.value, cnt = 0, self.counter.value

                bar.update(cnt)

