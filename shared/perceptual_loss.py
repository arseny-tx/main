import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision


# VGG architecter, used for the perceptual loss using a pretrained VGG network
class VGG19(torch.nn.Module):
    def __init__(self, requires_grad=False):
        super().__init__()
        vgg_pretrained_features = torchvision.models.vgg19(pretrained=True).features
        self.slice1 = torch.nn.Sequential()
        self.slice2 = torch.nn.Sequential()
        self.slice3 = torch.nn.Sequential()
        self.slice4 = torch.nn.Sequential()
        self.slice5 = torch.nn.Sequential()
        for x in range(2):
            self.slice1.add_module(str(x), vgg_pretrained_features[x])
        for x in range(2, 7):
            self.slice2.add_module(str(x), vgg_pretrained_features[x])
        for x in range(7, 12):
            self.slice3.add_module(str(x), vgg_pretrained_features[x])
        for x in range(12, 21):
            self.slice4.add_module(str(x), vgg_pretrained_features[x])
        for x in range(21, 30):
            self.slice5.add_module(str(x), vgg_pretrained_features[x])
        if not requires_grad:
            for param in self.parameters():
                param.requires_grad = False

    def forward(self, X):
        h_relu1 = self.slice1(X)
        h_relu2 = self.slice2(h_relu1)
        h_relu3 = self.slice3(h_relu2)
        h_relu4 = self.slice4(h_relu3)
        h_relu5 = self.slice5(h_relu4)
        out = [h_relu1, h_relu2, h_relu3, h_relu4, h_relu5]
        return out


def gram_matrix(x):
    a, b, c, d = x.size()  # a=batch size(=1)
    # b=number of feature maps
    # (c,d)=dimensions of a f. map (N=c*d)

    features = x.view(a , b, c * d)  # resise F_XL into \hat F_XL

    G = torch.matmul(features, features.t())  # compute the gram product

    # we 'normalize' the values of the gram matrix
    # by dividing by the number of element in each feature maps.
    return G.div(b * c * d)

from torch.nn.functional import l1_loss
class GramLoss(nn.Module):

    def forward(self, x, y):

        assert x.size == y.size

        b, c, *_ = x.size
        x = gram_matrix(x).div(b)
        y = gram_matrix(y).div(b)

        return l1_loss(x, y).div(c*c)
        

# Perceptual loss that uses a pretrained VGG network
class VGGLoss(nn.Module):
    def __init__(self, gpu_ids, crit='l1'):
        super(VGGLoss, self).__init__()
        self.vgg = VGG19().cuda()
        if crit == 'l1':
            self.weights = [1.0 / 32, 1.0 / 16, 1.0 / 8, 1.0 / 4, 1.0]
            self.criterion = nn.L1Loss()
        elif crit == 'gram':
            self.weights = [1.0, 1.0, 1.0, 1.0, 1.0]
            self.criterion = GramLoss()



    def forward(self, x, y):
        x_vgg, y_vgg = self.vgg(x), self.vgg(y)
        loss = 0
        for i in range(len(x_vgg)):
            loss += self.weights[i] * self.criterion(x_vgg[i], y_vgg[i].detach())
        return loss

class TVLoss(nn.Module):
    """
    Compute total variation loss.
    Inputs:
    - img: PyTorch Variable of shape (1, 3, H, W) holding an input image.
    - tv_weight: Scalar giving the weight w_t to use for the TV loss.
    Returns:
    - loss: PyTorch Variable holding a scalar giving the total variation loss
      for img weighted by tv_weight.
    Source:
        https://github.com/chongyangma/cs231n/blob/master/assignments/assignment3/style_transfer_pytorch.py
    """
    def __init__(self, crit='l1'):
        if crit == 'l1':
            self.criterion = nn.L1Loss()
        elif crit == 'l2':
            self.criterion = nn.L2Loss()

    def forward(self, img):
        x = self.criterion(img[:,:,:,:-1], img[:,:,:,1:]) \
            + self.criterion(img[:,:,:-1,:], img[:,:,1:,:])

        return x / np.product(img.shape)
     


