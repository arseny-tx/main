from PIL import Image, ImageFont, ImageDraw

from matplotlib.font_manager import FontManager, FontProperties
from scipy.spatial import distance as dist

import numpy as np
import cv2

from .datasets.masks import mask_from_poly



def order_points(pts):
	# https://www.pyimagesearch.com/2016/03/21/ordering-coordinates-clockwise-with-python-and-opencv/
	# sort the points based on their x-coordinates
	xSorted = pts[np.argsort(pts[:, 0]), :]
 
	# grab the left-most and right-most points from the sorted
	# x-roodinate points
	leftMost = xSorted[:2, :]
	rightMost = xSorted[2:, :]
 
	# now, sort the left-most coordinates according to their
	# y-coordinates so we can grab the top-left and bottom-left
	# points, respectively
	leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
	(tl, bl) = leftMost
 
	# now that we have the top-left coordinate, use it as an
	# anchor to calculate the Euclidean distance between the
	# top-left and right-most points; by the Pythagorean
	# theorem, the point with the largest distance will be
	# our bottom-right point
	D = dist.cdist(tl[np.newaxis], rightMost, "euclidean")[0]
	(br, tr) = rightMost[np.argsort(D)[::-1], :]
 
	# return the coordinates in top-left, top-right,
	# bottom-right, and bottom-left order
	return np.array([tl, tr, br, bl], dtype="float32")

def project_text(image_size, text, poly_np, font=None):
    
    bg_color = (255,255,255)

    if font is None:
    	# file = FontManager().findfont(FontProperties())
    	# font = ImageFont.truetype(file, size=50)
        font = ImageFont.load_default()

    w, h = font.getsize(text)

    image = Image.new("RGB", (w + 10,h + 10), color=bg_color)

    ImageDraw.Draw(image).text((10, 10), text, (0,0,0), font=font)
    
    image = image.crop( (10, 10, w+10, h+10) ) 
    # return image
    src_poly_np = np.array([[0, 0], [h+10, 0], [h+10, w+10],  [0, w+10]])

    h, status = cv2.findHomography(order_points(src_poly_np[:, [1, 0]]), order_points(poly_np))
    
    return cv2.warpPerspective(np.array(image), h, image_size, borderValue=bg_color)


def project_image(image, dst_poly_np):
	w, h = image.size

	src_poly_np = np.array([[0, 0], [h, 0], [h, w],  [0, w]])

	h, status = cv2.findHomography(order_points(src_poly_np[:, [1, 0]]), order_points(poly_np))

	return cv2.warpPerspective(np.array(image), h, image_size, borderValue=bg_color)	

def inpaint_text(img, text, poly_np):
    img_size = (img.shape[1], img.shape[0])
    mask = mask_from_poly(img_size, poly_np)
    proj = project_text(img_size, text, poly_np)
    
    img = img.transpose(2, 0, 1)
    proj = proj.transpose(2, 0, 1)
    
    return (img * ( 1 - mask ) + proj * mask).transpose(1, 2, 0)


import cv2
import skimage.transform  as tf


def crop_image(img_np, poly_np):
    # print(img_np)
    ## (1) Crop the bounding rect
    rect = cv2.boundingRect(poly_np)
    
    x,y,w,h = rect
    croped = img_np[y:y+h, x:x+w].copy()

    ## (2) make mask
    poly_np = poly_np - poly_np.min(axis=0)

    mask = np.zeros(croped.shape[:2], np.uint8)
    print(poly_np, type(poly_np), poly_np.shape, mask.shape, rect, croped.shape)
    cv2.drawContours(mask, [poly_np], -1, (255, 255, 255), -1, cv2.LINE_AA)

    ## (3) do bit-op
    dst = cv2.bitwise_and(croped, croped, mask=mask)
    
    return dst, poly_np

def extract_text(img, poly_np, size=(300, 50), pad=10):

	if isinstance(img, np.ndarray):
		return_pil = False		
	else:
		return_pil = True
		img = np.array(img)
		print(img)

	w, h = size

	img_width, img_height = img.shape[:-1]

	if w == None:
		w = int(img_width * (h / img_height))
	elif h == None:
		h = int(img_height * (w / img_width))

	img, pts = crop_image(img, poly_np)

	if img is None:
		return None, None

	mk_dst_poly = lambda w, h: np.array([[0, 0], [0, h], [w, h], [w, 0]])

	tform3 = tf.ProjectiveTransform()
	src = np.stack([pts[0], pts[3], pts[2], pts[1]])
	if tform3.estimate(mk_dst_poly(w, h), src):        
		img = tf.warp(img, tform3, output_shape=(h, w)) 
		img = (img * 255).astype(np.uint8)
		if return_pil:
			return Image.fromarray(img), img.size
		else:
			return img, img.size

	else:
	    return None, None   
    







